# Práctica 1

## Parte 1

### Script ab

Para ejecutar el script y que guarde el la hora del servidor y no la de nuestra maquina son necesarios los siguiente pasos:

* Desde `jair` lanzamos el comando `ssh-keygen -t rsa -b 2048` para generar nuestras claves, dejamos todo por defecto.
* Ahora tenemos que añadir nuestra clave al servidor al que nos queremos conectar sin contraseña `ssh-copy-id -i ~/.ssh/id_rsa.pub usuario@virtual.lab.inf.uva.es -p 31141`, tras este comando nos pedira introducir la contraseña del servidor.
* Llegados a este punto comprobamos que nos podemos conectar por ssh sin necesidad de clave `ssh usuario@virtual.lab.inf.uva.es -p 31141`.
* El script ya esta configurado para acceder a esta cuenta y realizar los date que se guardaran en sistema cliente.


En scriptMonitorRecursos se encuentran los scripts utilizados para la obtencion resumida de los datos de la monitorizacion de recursos en el servidor. Teniendo en la carpeta ./data :

* Ficheros binarios procedentes de SAR, nombrados de la forma "binarioX" donde X es el numero de la pagina.

* Ficheros con las horas de inicio y fin de cada caso de prueba realizado. Deben tener el nombre "logServerX" donde X es el numero de la pagina.

Ejecutar lo siguiente: $ bash script.sh ; bash tiempos.sh;

De esta forma se crearan ficheros con datos de recursos resumidos, diferenciando tipo de recursos, cada uno en una carpeta.


## Second part

All requirements and installation instructions are on the final report document. This is only for load tests execution.

### 1. Server-side
Execute these commands on server.
```
./startAgent.sh
./ngrok tcp 4444
```

Here, we get the port offered by ngrok: `tcp://0.tcp.ngrok.io:PORTNUMBER`

### 2. Client-side
On `practice-1/part-2`, start `scriptJMeter` script specifying port number offered by `ngrok` on server-side as argument.
```
./scriptJMeter PORTNUMBER
```

