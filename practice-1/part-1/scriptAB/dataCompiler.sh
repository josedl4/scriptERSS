#! /bin/bash
declare -r t="30";
declare -a url=("0" "http://virtual.lab.inf.uva.es:31142/pages/num1.html" "http://virtual.lab.inf.uva.es:31142/pages/num2.html" "http://virtual.lab.inf.uva.es:31142/pages/num3.html" "http://virtual.lab.inf.uva.es:31142/pages/num4.php" "http://virtual.lab.inf.uva.es:31142/pages/num5.py");
declare -a con=("0" "10" "100" "100" "500" "100" "500" "100" "500" "1000" "100" "500" "1000");
declare -a num=("0" "1000" "1000" "5000" "5000" "10000" "10000" "100000" "100000" "100000" "200000" "200000" "200000");

echo "Running test on the url: " ${url[$1]}
echo $1

ssh usuario@virtual.lab.inf.uva.es -p 31141 date
echo "------------- Begin test: -------------"

sleep $t

for i in `seq 1 12`; do
    echo "%%% IC"$i" Start %%%"

    for j in `seq 1 5`; do
        echo "%%% IC"$i" - "$j" iteration %%%"
        ssh usuario@virtual.lab.inf.uva.es -p 31141 date 
	# parte server
	texto=$(ssh usuario@virtual.lab.inf.uva.es -p 31141 date '+%d/%m/%Y')
	texto2=$(ssh usuario@virtual.lab.inf.uva.es -p 31141 date '+%H:%M:%S')
	echo "start-IC $i iteration $j" >>logServer
	echo "$texto $texto2" >>logServer
	# -----------
        ab -e num$1e-IC$i-n$j.csv -g num$1g-IC$i-n$j.data -k -n ${num[$i]} -c ${con[$i]} ${url[$1]}
	# parte server
	texto=$(ssh usuario@virtual.lab.inf.uva.es -p 31141 date '+%d/%m/%Y')
	texto2=$(ssh usuario@virtual.lab.inf.uva.es -p 31141 date '+%H:%M:%S')
	echo "end-IC $i iteration $j" >>logServer
	echo "$texto $texto2" >>logServer
	# -----------
        ssh usuario@virtual.lab.inf.uva.es -p 31141 date
        sleep $t
    done
    echo "%%% IC"$i" End %%%"
done
