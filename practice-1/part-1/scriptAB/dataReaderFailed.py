import sys
import numpy as np      # Importamos las librerias para  
import scipy as sp      # usar funciones estadisticas
import scipy.stats

import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

# Funcion para calcular el intervalo de confianza de un valor dado o 95% por defecto
# Lo calcula gracias a que se basa en usar la media y el error estantad de esta.
def mean_confidence_interval(data, confidence=0.95):
    a = 1.0*np.array(data)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * sp.stats.t._ppf((1+confidence)/2., n-1)
    r=[m, m-h, m+h]
    return r

# Funcion que comprueba si puede convertir los strings a float
def strToFloat(x):
    try:
        return float(x)
    except ValueError:
        pass

def isPercent(x):
    if '%' in x:
        return x    

def writeDataInCSV(data, nameFile="data.csv"):
    f = open(nameFile, 'w')
    f.write('Prueba; Total Solicitudes; Fallidas; 50%; 75%; 97%; 98%; 99%; 100%; Total Solicitudes; Fallidas; 50%; 75%; 97%; 98%; 99%; 100%; Total Solicitudes; Fallidas; 50%; 75%; 97%; 98%; 99%; 100%; Total Solicitudes; Fallidas; 50%; 75%; 97%; 98%; 99%; 100%; Total Solicitudes; Fallidas; 50%; 75%; 97%; 98%; 99%; 100%; Total Solicitudes; Fallidas; 50%; 75%; 97%; 98%; 99%; 100%; \n')
    c = 1
    for li in data:
        f.write('IC ' + str(c)+';')
        c += 1
        for el in li:
            f.write(str(float(el)) + ";")
        f.write("\n")

def num(s):
    try:
        return int(s)
    except ValueError:
        pass

# Funcion principal
def main():

    arrayICnum = [1000, 1000, 5000, 5000, 10000, 10000, 100000, 100000, 100000, 200000, 200000, 200000]
    arrayICcon = [10, 100, 100, 500, 100, 500, 100, 500, 1000, 100, 500, 1000]
    if(len(sys.argv) < 2):
        nameOfFile = 'num1-client.txt'
    else:
        nameOfFile = sys.argv[1]

    timePerRequest = []

    datafile = open(nameOfFile, 'r')
    num = 1
    for line in datafile:
        if('%%% IC'+str(num)+" Start" in line or 'SET IC'+str(num)+" :" in line ):
            if(num != 1):
                timePerRequest.append(tmp)
                if(len(tmp) != 40):
                    print("Error en el IC"+str(num-1))
            tmp = []
            #tmp.append('SET IC'+str(num)+":")
            num += 1
        
        if('Complete requests:' in line):
            floats = list(filter(strToFloat, line.split(' ')))
            tmp.append(floats[0])
            

        if('Failed requests:' in line):
            a = line.split(' ')
            a[-1] = a[-1].strip()
            tmp.append(a[-1])
            print('All')

        if('50%' in line or '75%' in line or '95%' in line or '98%' in line or '99%' in line or '100%' in line):
            floats = list(filter(strToFloat, line.split(' ')))
            percents = list(filter(isPercent, line.split(' ')))
            print(percents[0],floats[0])
            tmp.append(floats[0])
    if(len(tmp) != 40):
                    print("Error en el IC"+str(num-1))
    timePerRequest.append(tmp)


    if(len(sys.argv) == 3):
        writeDataInCSV(timePerRequest, sys.argv[2])
    else:
        writeDataInCSV(timePerRequest)


if __name__ == "__main__": main()
