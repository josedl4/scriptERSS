import sys
import numpy as np      # Importamos las librerias para  
import scipy as sp      # usar funciones estadisticas
import scipy.stats

import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

# Funcion para calcular el intervalo de confianza de un valor dado o 95% por defecto
# Lo calcula gracias a que se basa en usar la media y el error estantad de esta.
def mean_confidence_interval(data, confidence=0.95):
    a = 1.0*np.array(data)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * sp.stats.t._ppf((1+confidence)/2., n-1)
    r=[m, m-h, m+h]
    return r

# Funcion que comprueba si puede convertir los strings a float
def strToFloat(x):
    try:
        return float(x)
    except ValueError:
        pass

def writeDataInCSV(data, stats, nameFile="data.csv"):
    f = open(nameFile, 'w')
    f.write('Prueba; Test 1; Test 2; Test 3; Test 4; Test 5; Mean; Ext.Inferior; Ext. Superior \n')
    c = 1
    for li in data:
        f.write('IC ' + str(c)+';')
        c += 1
        for el in li:
            f.write(str(float(el)) + ";")
        f.write(str(stats[data.index(li)][0]) + ";")
        f.write(str(stats[data.index(li)][1]) + ";" + str(stats[data.index(li)][2])+"\n")

# Funcion principal
def main():

    arrayICnum = [1000, 1000, 5000, 5000, 10000, 10000, 100000, 100000, 100000, 200000, 200000, 200000]
    arrayICcon = [10, 100, 100, 500, 100, 500, 100, 500, 1000, 100, 500, 1000]
    if(len(sys.argv) < 2):
        nameOfFile = 'num1-client.txt'
    else:
        nameOfFile = sys.argv[1]

    timePerRequest = []

    datafile = open(nameOfFile, 'r')
    num = 1
    for line in datafile:
        if('%%% IC'+str(num)+" Start" in line or 'SET IC'+str(num)+" :" in line ):
            if(num != 1):
                timePerRequest.append(tmp)
                if(len(tmp) != 5):
                    print("Error en el IC"+str(num-1))
            tmp = []
            #tmp.append('SET IC'+str(num)+":")
            num += 1

        if('50%' in line):
            floats = list(filter(strToFloat, line.split(' ')))

            tmp.append(floats[0])
    if(len(tmp) != 5):
                    print("Error en el IC"+str(num-1))
    timePerRequest.append(tmp)

    listOfStats = []

    for li in timePerRequest:
        listOfStats.append(mean_confidence_interval(list(map(float, li))))

    if(len(sys.argv) == 3):
        writeDataInCSV(timePerRequest,listOfStats, sys.argv[2])
    else:
        writeDataInCSV(timePerRequest,listOfStats)



    listOfMeans = []
    for li in listOfStats:
        listOfMeans.append(li[0])

    mpl.rcParams['legend.fontsize'] = 10

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    
    print(arrayICnum)
    print(arrayICcon)
    print(listOfMeans)

    ax.plot(arrayICnum, arrayICcon, listOfMeans, label='Tiempo de respuesta')
    ax.legend()
    ax.set_xlabel('Numero de peticiones')
    ax.set_ylabel('Peticiones concurrentes')
    ax.set_zlabel('Tiempo de respuesta medio')

    plt.show()

if __name__ == "__main__": main()
