#! /bin/bash
date
echo "Begin test:"
for var in "$@"
do
    #echo "$var"

    echo "SET IC1: 10 CLIENTES 1000 PETICIONES --- $var"
    sleep 30
    date
    ab -k -n 1000 -c 10 $var
    date
    sleep 30
    date
    ab -k -n 1000 -c 10 $var
    date
    sleep 30
    date
    ab -k -n 1000 -c 10 $var
    date
    sleep 30
    date
    ab -k -n 1000 -c 10 $var
    date
    sleep 30
    date
    ab -k -n 1000 -c 10 $var
    date
    sleep 30
    echo SET IC1

    echo "SET IC2: 100 CLIENTES 1000 PETICIONES --- $var"
    date
    ab -k -n 1000 -c 100 $var
    date
    sleep 30
    date
    ab -k -n 1000 -c 100 $var
    date
    sleep 30
    date
    ab -k -n 1000 -c 100 $var
    date
    sleep 30
    date
    ab -k -n 1000 -c 100 $var
    date
    sleep 30
    date
    ab -k -n 1000 -c 100 $var
    date
    sleep 30
    echo SET IC2

    echo "SET IC3: 100 CLIENTES 5000 PETICIOENS"
    date
    ab -k -n 5000 -c 100 $var
    date
    sleep 30
    date
    ab -k -n 5000 -c 100 $var
    date
    sleep 30
    date
    ab -k -n 5000 -c 100 $var
    date
    sleep 30
    date
    ab -k -n 5000 -c 100 $var
    date
    sleep 30
    date
    ab -k -n 5000 -c 100 $var
    date
    sleep 30
    echo SET IC3

    echo "SET IC4: 500 CLIENTES 5000 PETICIONES --- $var"
    date
    ab -k -n 5000 -c 500 $var
    date
    sleep 30
    date
    ab -k -n 5000 -c 500 $var
    date
    sleep 30
    date
    ab -k -n 5000 -c 500 $var
    date
    sleep 30
    date
    ab -k -n 5000 -c 500 $var
    date
    sleep 30
    date
    ab -k -n 5000 -c 500 $var
    date
    sleep 30
    echo SET IC4

    echo "SET IC5: 100 CLIENTES 10000 PETICIONES --- $var"
    date
    ab -k -n 10000 -c 100 $var
    date
    sleep 30
    date
    ab -k -n 10000 -c 100 $var
    date
    sleep 30
    date
    ab -k -n 10000 -c 100 $var
    date
    sleep 30
    date
    ab -k -n 10000 -c 100 $var
    date
    sleep 30
    date
    ab -k -n 10000 -c 100 $var
    date
    sleep 30
    echo SET IC5

    echo "SET IC6: 500 CLIENTES 10000 PETICIONES --- $var"
    date
    ab -k -n 10000 -c 500 $var
    date
    sleep 30
    date
    ab -k -n 10000 -c 500 $var
    date
    sleep 30
    date
    ab -k -n 10000 -c 500 $var
    date
    sleep 30
    date
    ab -k -n 10000 -c 500 $var
    date
    sleep 30
    date
    ab -k -n 10000 -c 500 $var
    date
    sleep 30
    echo SET IC6

    echo "SET IC7: 100 CLIENTES 100000 PETICONES"
    date
    ab -k -n 100000 -c 100 $var
    date
    sleep 30
    date
    ab -k -n 100000 -c 100 $var
    date
    sleep 30
    date
    ab -k -n 100000 -c 100 $var
    date
    sleep 30
    date
    ab -k -n 100000 -c 100 $var
    date
    sleep 30
    date
    ab -k -n 100000 -c 100 $var
    date
    sleep 30
    echo SET IC7

    echo "SET IC8: 500 CLIENTES 100000 PETICIONES --- $var"
    date
    ab -k -n 100000 -c 500 $var
    date
    sleep 30
    date
    ab -k -n 100000 -c 500 $var
    date
    sleep 30
    date
    ab -k -n 100000 -c 500 $var
    date
    sleep 30
    date
    ab -k -n 100000 -c 500 $var
    date
    sleep 30
    date
    ab -k -n 100000 -c 500 $var
    date
    sleep 30
    echo SET IC8

    echo "SET IC9: 1000 CLIENTES 100000 PETICIONES --- $var"
    date
    ab -k -n 100000 -c 1000 $var
    date
    sleep 30
    date
    ab -k -n 100000 -c 1000 $var
    date
    sleep 30
    date
    ab -k -n 100000 -c 1000 $var
    date
    sleep 30
    date
    ab -k -n 100000 -c 1000 $var
    date
    sleep 30
    date
    ab -k -n 100000 -c 1000 $var
    date
    sleep 30
    echo SET IC9

    echo "SET IC10 : 100 CLIENTES 200000 PETICIONES --- $var"
    date
    ab -k -n 200000 -c 100 $var
    date
    sleep 30
    date
    ab -k -n 200000 -c 100 $var
    date
    sleep 30
    date
    ab -k -n 200000 -c 100 $var
    date
    sleep 30
    date
    ab -k -n 200000 -c 100 $var
    date
    sleep 30
    date
    ab -k -n 200000 -c 100 $var
    date
    sleep 30
    echo SET IC10

    echo "SET IC11: 500 CLIENTES 200000 PETICIONES --- $var"
    date
    ab -k -n 200000 -c 500 $var
    date
    sleep 30
    date
    ab -k -n 200000 -c 500 $var
    date
    sleep 30
    date
    ab -k -n 200000 -c 500 $var
    date
    sleep 30
    date
    ab -k -n 200000 -c 500 $var
    date
    sleep 30
    date
    ab -k -n 200000 -c 500 $var
    date
    sleep 30
    echo SET IC11

    echo "SET IC12: 1000 CLIENTES 200000 PETICIONES --- $var"
    date
    ab -k -n 200000 -c 1000 $var
    date
    sleep 30
    date
    ab -k -n 200000 -c 1000 $var
    date
    sleep 30
    date
    ab -k -n 200000 -c 1000 $var
    date
    sleep 30
    date
    ab -k -n 200000 -c 1000 $var
    date
    sleep 30
    date
    ab -k -n 200000 -c 1000 $var
    date

done
echo "END TEST"

