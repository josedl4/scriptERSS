

mkdir resultados
#Page1
python ./tools/dataDiv.py ./data/logServer1 ./data/binario1 u cpu
python3 ./tools/dataAnalyserSar.py cpu >./resultados/cpu1.csv
python ./tools/dataDiv.py ./data/logServer1 ./data/binario1 b io
python3 ./tools/dataAnalyserSar.py io >./resultados/io1.csv
python ./tools/dataDiv.py ./data/logServer1 ./data/binario1 r mem
python3 ./tools/dataAnalyserSar.py mem >./resultados/mem1.csv
python ./tools/dataDiv.py ./data/logServer1 ./data/binario1 w proc
python3 ./tools/dataAnalyserSar.py proc >./resultados/proc1.csv


#Page2
python ./tools/dataDiv.py ./data/logServer2 ./data/binario2 u cpu
python3 ./tools/dataAnalyserSar.py cpu >./resultados/cpu2.csv
python ./tools/dataDiv.py ./data/logServer2 ./data/binario2 b io
python3 ./tools/dataAnalyserSar.py io >./resultados/io2.csv
python ./tools/dataDiv.py ./data/logServer2 ./data/binario2 r mem
python3 ./tools/dataAnalyserSar.py mem >./resultados/mem2.csv
python ./tools/dataDiv.py ./data/logServer2 ./data/binario2 w proc
python3 ./tools/dataAnalyserSar.py proc >./resultados/proc2.csv

#Page3
python ./tools/dataDiv.py ./data/logServer3 ./data/binario3 u cpu
python3 ./tools/dataAnalyserSar.py cpu >./resultados/cpu3.csv
python ./tools/dataDiv.py ./data/logServer3 ./data/binario3 b io
python3 ./tools/dataAnalyserSar.py io >./resultados/io3.csv
python ./tools/dataDiv.py ./data/logServer3 ./data/binario3 r mem
python3 ./tools/dataAnalyserSar.py mem >./resultados/mem3.csv
python ./tools/dataDiv.py ./data/logServer3 ./data/binario3 w proc
python3 ./tools/dataAnalyserSar.py proc >./resultados/proc3.csv

#Page4
python ./tools/dataDiv.py ./data/logServer4 ./data/binario4 u cpu
python3 ./tools/dataAnalyserSar.py cpu >./resultados/cpu4.csv
python ./tools/dataDiv.py ./data/logServer4 ./data/binario4 b io
python3 ./tools/dataAnalyserSar.py io >./resultados/io4.csv
python ./tools/dataDiv.py ./data/logServer4 ./data/binario4 r mem
python3 ./tools/dataAnalyserSar.py mem >./resultados/mem4.csv
python ./tools/dataDiv.py ./data/logServer4 ./data/binario4 w proc
python3 ./tools/dataAnalyserSar.py proc >./resultados/proc4.csv

#Page5
python ./tools/dataDiv.py ./data/logServer5 ./data/binario5 u cpu
python3 ./tools/dataAnalyserSar.py cpu >./resultados/cpu5.csv
python ./tools/dataDiv.py ./data/logServer5 ./data/binario5 b io
python3 ./tools/dataAnalyserSar.py io >./resultados/io5.csv
python ./tools/dataDiv.py ./data/logServer5 ./data/binario5 r mem
python3 ./tools/dataAnalyserSar.py mem >./resultados/mem5.csv
python ./tools/dataDiv.py ./data/logServer5 ./data/binario5 w proc
python3 ./tools/dataAnalyserSar.py proc >./resultados/proc5.csv


# Load stats -q
# Network stats 
