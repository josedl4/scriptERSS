import os
import glob
import sys
import re

import numpy as np      # Importamos las librerias para  
import scipy as sp      # usar funciones estadisticas
import scipy.stats

#### README ####
#
# Actualmente funciona teniendo todos los csv generados sobre la cpu en una carpeta llamada cpu en 
# el mismo directorio y anadiendo el parametro cpu al programa
#
################

def mean_confidence_interval(data, confidence=0.95):
    a = 1.0*np.array(data)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * sp.stats.t._ppf((1+confidence)/2., n-1)
    r=[m, h]
    return r


def writeDataInCSV(data, nameFile="data.csv"):      ### Ejemplo para sacar los datos a fichero csv
    f = open(nameFile, 'w')
    f.write('Campos; De; Las; Tablas;\n')
    c = 1
    for li in data:
        f.write('IC ' + str(c)+';')
        c += 1
        for el in li:
            f.write(str(el) + ";")
        f.write("\n")



def strToCoordinates (s):
    ic = 0
    it = 0
    
    #r = re.compile("([A-Z]+)([0-9]+)([a-z]+)([0-9]+)")
    match = re.match(r"([A-Z]+)([0-9]+)([a-z]+)([0-9]+)", s, re.M|re.I)
    #= re.match(r"([a-zA-Z]+)([0-9]+)([a-zA-Z]+)([0-9]+)", s, re.I)
    
    items = match.groups()
    result = []
    result.append(items[1])
    result.append(items[3])
    return result

def changeCommaToPointFloat(s):
    if(s.find(",") != -1): 
        tmp = s.split(",")
        tmp = tmp[0]+"."+tmp[1]
    else:
        tmp = s+".0"
    return float(tmp)

# Funcion principal
def main():

     

    if(sys.argv[1] == "cpu"):       ### Segun el primer parametro cargara una ruta donde obtener los archivos
        os.chdir("./cpu")           ### importante que no haya otro tipo de archivos con otras estructuras

        table1 = [[0.0 for x in range(5)] for y in range(12)]
        table2 = [[0.0 for x in range(5)] for y in range(12)]

        for f in glob.glob("*"):
            datafile = open(f, 'r')
            for line in datafile:
                if("Media:" in line):
                    coor = []
                    coor = strToCoordinates(f)
                    dataValues = line.split()
                    table1[int(coor[0])-1][int(coor[1])-1] = changeCommaToPointFloat(dataValues[2]) ### Valores de las columnas de datos
                    table2[int(coor[0])-1][int(coor[1])-1] = changeCommaToPointFloat(dataValues[4]) ### que queremos recoger    
                    break                                                                           ### Tendra que depender del argumentoo pasado al script
        
        tableOfMeans1 = [[0.0 for x in range(2)] for y in range(12)]
        tableOfMeans2 = [[0.0 for x in range(2)] for y in range(12)]

        for li in range(len(tableOfMeans1)):                ### Generamos las medias e intervalos para cada tabla
            tmp = mean_confidence_interval(table1[li])      ### se podria cambiar para realizarlo todo en una si es necesario
            tableOfMeans1[li][0] = tmp[0]
            tableOfMeans1[li][1] = tmp[1]
            tmp = mean_confidence_interval(table2[li])
            tableOfMeans2[li][0] = tmp[0]
            tableOfMeans2[li][1] = tmp[1]
 	### Escritura de resultados
        print('Casos;Ite1;Ite2;Ite3;Ite4;Ite5;%usr;IC %usr;Ite1;Ite2;Ite3;Ite4;Ite5;%sys;IC %sys;')
        c = 1
        for linea in range(len(tableOfMeans1)):
                print('IC' + str(c)+";"+str(table1[linea][0])+';'+str(table1[linea][1])+';'+str(table1[linea][2])+';'+str(table1[linea][3])+';'+str(table1[linea][4])+';'+str(tableOfMeans1[linea][0])+';'+str(tableOfMeans1[linea][1])+';'+str(table2[linea][0])+';'+str(table2[linea][1])+';'+str(table2[linea][2])+';'+str(table2[linea][3])+';'+str(table2[linea][4])+';'+str(tableOfMeans2[linea][0])+';'+str(tableOfMeans2[linea][1])+';')
                c += 1	

    elif (sys.argv[1] == "mem"):
        os.chdir("./mem")

        table1 = [[0.0 for x in range(5)] for y in range(12)]
        table2 = [[0.0 for x in range(5)] for y in range(12)]
        table3 = [[0.0 for x in range(5)] for y in range(12)]

        for f in glob.glob("*"):
            datafile = open(f, 'r')
            for line in datafile:
                if("Media:" in line):
                    coor = []
                    coor = strToCoordinates(f)
                    dataValues = line.split()
                    table1[int(coor[0])-1][int(coor[1])-1] = changeCommaToPointFloat(dataValues[1]) ### Valores de las columnas de datos
                    table2[int(coor[0])-1][int(coor[1])-1] = changeCommaToPointFloat(dataValues[2]) ### que queremos recoger   
                    table3[int(coor[0])-1][int(coor[1])-1] = changeCommaToPointFloat(dataValues[3]) 
                    break                                                                           ### Tendra que depender del argumentoo pasado al script
        
        tableOfMeans1 = [[0.0 for x in range(2)] for y in range(12)]
        tableOfMeans2 = [[0.0 for x in range(2)] for y in range(12)]
        tableOfMeans3 = [[0.0 for x in range(2)] for y in range(12)]

        for li in range(len(tableOfMeans1)):                ### Generamos las medias e intervalos para cada tabla
            tmp = mean_confidence_interval(table1[li])      ### se podria cambiar para realizarlo todo en una si es necesario
            tableOfMeans1[li][0] = tmp[0]
            tableOfMeans1[li][1] = tmp[1]
            tmp = mean_confidence_interval(table2[li])
            tableOfMeans2[li][0] = tmp[0]
            tableOfMeans2[li][1] = tmp[1]
            tmp = mean_confidence_interval(table3[li])
            tableOfMeans3[li][0] = tmp[0]
            tableOfMeans3[li][1] = tmp[1]

 	### Escritura de resultados
        print('Casos;Ite1;Ite2;Ite3;Ite4;Ite5;memfree;IC memfree;Ite1;Ite2;Ite3;Ite4;Ite5;memused;IC memused;Ite1;Ite2;Ite3;Ite4;Ite5;%memused;IC %memused')
        c = 1
        for linea in range(len(tableOfMeans1)):
                print('IC' + str(c)+";"+str(table1[linea][0])+';'+str(table1[linea][1])+';'+str(table1[linea][2])+';'+str(table1[linea][3])+';'+str(table1[linea][4])+';'+str(tableOfMeans1[linea][0])+';'+str(tableOfMeans1[linea][1])+';'+str(table2[linea][0])+';'+str(table2[linea][1])+';'+str(table2[linea][2])+';'+str(table2[linea][3])+';'+str(table2[linea][4])+';'+str(tableOfMeans2[linea][0])+';'+str(tableOfMeans2[linea][1])+';'+str(table3[linea][0])+';'+str(table3[linea][1])+';'+str(table3[linea][2])+';'+str(table3[linea][3])+';'+str(table3[linea][4])+';'+str(tableOfMeans3[linea][0])+';'+str(tableOfMeans3[linea][1])+';')
                c += 1
        

    elif (sys.argv[1] == "io"):
        os.chdir("./io")

        table1 = [[0.0 for x in range(5)] for y in range(12)]
        table2 = [[0.0 for x in range(5)] for y in range(12)]
        table3 = [[0.0 for x in range(5)] for y in range(12)]

        for f in glob.glob("*"):
            datafile = open(f, 'r')
            for line in datafile:
                if("Media:" in line):
                    coor = []
                    coor = strToCoordinates(f)
                    dataValues = line.split()
                    table1[int(coor[0])-1][int(coor[1])-1] = changeCommaToPointFloat(dataValues[1]) ### Valores de las columnas de datos
                    table2[int(coor[0])-1][int(coor[1])-1] = changeCommaToPointFloat(dataValues[2]) ### que queremos recoger   
                    table3[int(coor[0])-1][int(coor[1])-1] = changeCommaToPointFloat(dataValues[3]) 
                    break                                                                           ### Tendra que depender del argumentoo pasado al script
        
        tableOfMeans1 = [[0.0 for x in range(2)] for y in range(12)]
        tableOfMeans2 = [[0.0 for x in range(2)] for y in range(12)]
        tableOfMeans3 = [[0.0 for x in range(2)] for y in range(12)]

        for li in range(len(tableOfMeans1)):                ### Generamos las medias e intervalos para cada tabla
            tmp = mean_confidence_interval(table1[li])      ### se podria cambiar para realizarlo todo en una si es necesario
            tableOfMeans1[li][0] = tmp[0]
            tableOfMeans1[li][1] = tmp[1]
            tmp = mean_confidence_interval(table2[li])
            tableOfMeans2[li][0] = tmp[0]
            tableOfMeans2[li][1] = tmp[1]
            tmp = mean_confidence_interval(table3[li])
            tableOfMeans3[li][0] = tmp[0]
            tableOfMeans3[li][1] = tmp[1]

 	### Escritura de resultados
        print('Casos;Ite1;Ite2;Ite3;Ite4;Ite5;tps;IC tps;Ite1;Ite2;Ite3;Ite4;Ite5;rtps;IC rtps;Ite1;Ite2;Ite3;Ite4;Ite5;wtps;IC wtps')
        c = 1
        for linea in range(len(tableOfMeans1)):
                print('IC' + str(c)+";"+str(table1[linea][0])+';'+str(table1[linea][1])+';'+str(table1[linea][2])+';'+str(table1[linea][3])+';'+str(table1[linea][4])+';'+str(tableOfMeans1[linea][0])+';'+str(tableOfMeans1[linea][1])+';'+str(table2[linea][0])+';'+str(table2[linea][1])+';'+str(table2[linea][2])+';'+str(table2[linea][3])+';'+str(table2[linea][4])+';'+str(tableOfMeans2[linea][0])+';'+str(tableOfMeans2[linea][1])+';'+str(table3[linea][0])+';'+str(table3[linea][1])+';'+str(table3[linea][2])+';'+str(table3[linea][3])+';'+str(table3[linea][4])+';'+str(tableOfMeans3[linea][0])+';'+str(tableOfMeans3[linea][1])+';')
                c += 1	

    elif (sys.argv[1] == "util"):
        os.chdir("./util")
        table1 = [[0.0 for x in range(5)] for y in range(12)]

        for f in glob.glob("*"):
            datafile = open(f, 'r')
            for line in datafile:
                if("Media:" in line):
                    coor = []
                    coor = strToCoordinates(f)
                    dataValues = line.split()
                    table1[int(coor[0])-1][int(coor[1])-1] = changeCommaToPointFloat(dataValues[2]) ### Valores de las columnas de datos
                    break                                                                           ### Tendra que depender del argumentoo pasado al script
        
        tableOfMeans1 = [[0.0 for x in range(2)] for y in range(12)]
        tableOfMeans2 = [[0.0 for x in range(2)] for y in range(12)]

        for li in range(len(tableOfMeans1)):                ### Generamos las medias e intervalos para cada tabla
            tmp = mean_confidence_interval(table1[li])      ### se podria cambiar para realizarlo todo en una si es necesario
            tableOfMeans1[li][0] = tmp[0]
            tableOfMeans1[li][1] = tmp[1]
 	### Escritura de resultados
        print('Casos;Ite1;Ite2;Ite3;Ite4;Ite5;%util;IC %util;')
        c = 1
        for linea in range(len(tableOfMeans1)):
                print('IC' + str(c)+";"+str(table1[linea][0])+';'+str(table1[linea][1])+';'+str(table1[linea][2])+';'+str(table1[linea][3])+';'+str(table1[linea][4])+';'+str(tableOfMeans1[linea][0])+';'+str(tableOfMeans1[linea][1])+';')
                c += 1	

    else:
        os.chdir("./proc")

        table1 = [[0.0 for x in range(5)] for y in range(12)]
        table2 = [[0.0 for x in range(5)] for y in range(12)]

        for f in glob.glob("*"):
            datafile = open(f, 'r')
            for line in datafile:
                if("Media:" in line):
                    coor = []
                    coor = strToCoordinates(f)
                    dataValues = line.split()
                    table1[int(coor[0])-1][int(coor[1])-1] = changeCommaToPointFloat(dataValues[1]) ### Valores de las columnas de datos
                    table2[int(coor[0])-1][int(coor[1])-1] = changeCommaToPointFloat(dataValues[2]) ### que queremos recoger    
                    break                                                                           ### Tendra que depender del argumentoo pasado al script
        
        tableOfMeans1 = [[0.0 for x in range(2)] for y in range(12)]
        tableOfMeans2 = [[0.0 for x in range(2)] for y in range(12)]

        for li in range(len(tableOfMeans1)):                ### Generamos las medias e intervalos para cada tabla
            tmp = mean_confidence_interval(table1[li])      ### se podria cambiar para realizarlo todo en una si es necesario
            tableOfMeans1[li][0] = tmp[0]
            tableOfMeans1[li][1] = tmp[1]
            tmp = mean_confidence_interval(table2[li])
            tableOfMeans2[li][0] = tmp[0]
            tableOfMeans2[li][1] = tmp[1]
 	### Escritura de resultados
        print('Casos;Ite1;Ite2;Ite3;Ite4;Ite5;proc/s;IC proc/s;Ite1;Ite2;Ite3;Ite4;Ite5;cswch/s;IC  cswch/s;')
        c = 1
        for linea in range(len(tableOfMeans1)):
                print('IC' + str(c)+";"+str(table1[linea][0])+';'+str(table1[linea][1])+';'+str(table1[linea][2])+';'+str(table1[linea][3])+';'+str(table1[linea][4])+';'+str(tableOfMeans1[linea][0])+';'+str(tableOfMeans1[linea][1])+';'+str(table2[linea][0])+';'+str(table2[linea][1])+';'+str(table2[linea][2])+';'+str(table2[linea][3])+';'+str(table2[linea][4])+';'+str(tableOfMeans2[linea][0])+';'+str(tableOfMeans2[linea][1])+';')
                c += 1	
    

    


if __name__ == "__main__": main()
