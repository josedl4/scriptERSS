import os
import sys
from datetime import datetime, timedelta

# Funcion principal
def main():

    nameOfFile = sys.argv[1]
    nameOfBin = sys.argv[2]
    paramSar = sys.argv[3]
    carpeta = sys.argv[4]

    datafile = open(nameOfFile, 'r')
    started = False
    nameOfDestFile = ""
    startTime = ""
    endTime = ""

    print('mkdir '+ carpeta)
    os.system('mkdir '+ carpeta)
    
    counter = 0
    for line in datafile:
        counter += 1
        if('start-' in line):
            if(started): raise Exception('Fallo estructural del fichero de entrada.')
            tmp = list(line.split('-'))
            nameOfDestFile = "".join(tmp[1].split()).strip()
            #nameOfDestFile = tmp[1].remplace(" ", "")
            started = True
            continue

        if('end-' in line):
            if(not started): raise Exception('Fallo estructural del fichero de entrada.')
            started = False
            continue

        if(started):
            tmp = list(line.split(" "))
            startTime = tmp[1].strip()

        else:
            tmp = list(line.split(" "))
            endTime = tmp[1].strip()
            formato = "%H:%M:%S"
            dateEmpieza = datetime.strptime(startTime,formato)
            dateAcaba = datetime.strptime(endTime,formato)
            if (counter < 21):
                 dateEmpieza = dateEmpieza - timedelta(seconds=1)
                 dateAcaba = dateAcaba + timedelta(seconds=1)
            startTime = datetime.strftime(dateEmpieza, formato)
            endTime = datetime.strftime(dateAcaba, formato)
            command = 'sar -' + paramSar + ' -f ' + nameOfBin + ' -s ' + startTime + ' -e ' + endTime + ' > ' + carpeta + '/' + nameOfDestFile
            print(command)
            os.system(command)


if __name__ == "__main__": main()
