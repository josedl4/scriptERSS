#!/bin/bash

declare -a arr=("10" "20" "30" "50" "80" "130" "210" "340" "550") 

for i in "${arr[@]}"
do
	jmeter -n -t webcalendar-performance-testing-planLigera.jmx -Juser=$i -l 1-$i.jtl
	sleep 60
done

for i in "${arr[@]}"
do
	jmeter -n -t webcalendar-performance-testing-planPesada.jmx -Juser=$i -l 1-$i.jtl
	sleep 60
done
